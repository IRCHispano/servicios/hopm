
_Tareas a realizar:_

* Crear un configurador fácil al estilo del IRCh o ServicesIRCh.
* Agregar soporte de mySQL, json y maxmind geoip 2.
* Implementar opción trazabilidad de las Glines por Proxy y por DNSBL mediante SQL.
* Implementar opción de ejecutar como Service en P10 en vez de Cliente.
* Ejecutando como service, poder especificar que servidores se escanearan/o no se escanean a sus usuarios. 
